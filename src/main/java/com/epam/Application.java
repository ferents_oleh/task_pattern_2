package com.epam;

import com.epam.controller.impl.BoardControllerImpl;
import com.epam.model.Board;
import com.epam.view.Menu;

public class Application {
    public static void main(String[] args) {
        new Menu(
                new BoardControllerImpl(
                        new Board()
                )
        );
    }
}
