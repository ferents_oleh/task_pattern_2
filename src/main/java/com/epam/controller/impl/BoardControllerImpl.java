package com.epam.controller.impl;

import com.epam.controller.BoardController;
import com.epam.model.Board;
import com.epam.model.Task;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class BoardControllerImpl implements BoardController {
    private Board board;

    public BoardControllerImpl(Board board) {
        this.board = board;
    }

    @Override
    public void addTask() {
        board.addTask();
    }

    @Override
    public void addTaskToInProgress() {
        board.toInProgress();
    }

    @Override
    public void addTaskToCodeReview() {
        board.codeReview();
    }

    @Override
    public void addTaskToDone() {
        board.toDone();
    }

    @Override
    public void printTasks() {
        if (board.getTasks().size() > 0) {
            for (Task task : board.getTasks()) {
                log.info("Id: " + task.getId());
                log.info("Name: " + task.getName());
                log.info("Description: " + task.getDescription());
                log.info("Status: " + task.getEnumType());
            }
        } else {
            log.info("There is no tasks!");
        }
    }
}
