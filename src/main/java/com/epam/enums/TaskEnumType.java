package com.epam.enums;

public enum TaskEnumType {
    ToDo, InProgress, CodeReview, Done
}
