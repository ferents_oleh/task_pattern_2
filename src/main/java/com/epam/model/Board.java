package com.epam.model;

import com.epam.model.impl.NewTaskState;
import com.epam.model.impl.ToInProgress;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Log4j2
@Getter
@Setter
public class Board {
    private List<Task> tasks;

    private int taskCounter;

    public Board() {
        tasks = new ArrayList<>();
        taskCounter = 1;
    }

    public void addTask() {
        log.info("Enter task data: ");
        Scanner scanner = new Scanner(System.in);
        log.info("Name: ");
        String name = scanner.nextLine();
        log.info("Description: ");
        String description = scanner.nextLine();
        TaskState state = new NewTaskState();
        Task newTask = new Task(taskCounter, name, description, null);
        tasks.add(newTask);
        state.addTask(newTask);
        taskCounter++;
    }

    public void toInProgress() {
        if (tasks.isEmpty()) {
            log.info("There is no tasks!");
        } else {
            int id = getTaskId();
            Task task = tasks.get(id);
            task.getState().toInProgress(task);
        }
    }

    public void codeReview() {
        if (tasks.isEmpty()) {
            log.info("There is no tasks!");
        } else {
            int id = getTaskId();
            Task task = tasks.get(id);
            task.getState().codeReview(task);
        }
    }

    public void toDone() {
        if (tasks.isEmpty()) {
            log.info("There is no done tasks!");
        } else {
            int id = getTaskId();
            Task task = tasks.get(id);
            task.getState().toDone(task);
        }
    }

    private int getTaskId() {
        Scanner scanner = new Scanner(System.in);
        log.info("Enter task id: ");
        int id = Integer.parseInt(scanner.nextLine());
        id--;
        return id;
    }
}
