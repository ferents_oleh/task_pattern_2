package com.epam.model.impl;

import com.epam.enums.TaskEnumType;
import com.epam.model.Task;
import com.epam.model.TaskState;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class CodeReview implements TaskState {
    @Override
    public void toDone(Task task) {
        log.info("Task " + task.getName() +" is done");
        task.setState(new ToDone());
        task.setEnumType(TaskEnumType.Done);
    }

    @Override
    public void toInProgress(Task task) {
        log.info("Task " + task.getName() + " in progress");
        task.setState(new ToInProgress());
        task.setEnumType(TaskEnumType.InProgress);
    }

    @Override
    public void addTask(Task task) {
        log.info("Task " + task.getName() + " in to do");
        task.setState(new NewTaskState());
        task.setEnumType(TaskEnumType.ToDo);
    }
}
