package com.epam.model.impl;

import com.epam.model.TaskState;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ToDone implements TaskState { }
