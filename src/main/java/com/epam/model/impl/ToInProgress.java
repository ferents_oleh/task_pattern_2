package com.epam.model.impl;

import com.epam.enums.TaskEnumType;
import com.epam.model.Task;
import com.epam.model.TaskState;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ToInProgress implements TaskState {
    @Override
    public void codeReview(Task task) {
        log.info("Task " + task.getName() + " in code review");
        task.setState(new CodeReview());
        task.setEnumType(TaskEnumType.CodeReview);
    }

    @Override
    public void addTask(Task task) {
        log.info("Task " + task.getName() + " in To-Do");
        task.setState(new NewTaskState());
        task.setEnumType(TaskEnumType.ToDo);
    }
}
