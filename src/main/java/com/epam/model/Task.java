package com.epam.model;

import com.epam.enums.TaskEnumType;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Task {
    private int id;

    private String name;

    private String description;

    private TaskState state;

    private TaskEnumType enumType;

    public Task(int id, String name, String description, TaskState state) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.state = state;
    }
}
